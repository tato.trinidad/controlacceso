/**
 * 
 */
package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.Evento;
import py.edu.ucsa.controlacceso.core.services.EventoService;

/**
 * @author halfonso
 *
 */
@RestController
@RequestMapping("evento")
public class EventoController {

	
	@Autowired
	private EventoService eventoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Evento>> listar() {
		List<Evento> respuesta = eventoService.listar();
		return new ResponseEntity<List<Evento>>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(value = "{idEvento}", method = RequestMethod.GET)
	public ResponseEntity<Evento> listarById(@PathVariable("idEvento") Long idEvento) {
		Evento respuesta = eventoService.listarById(idEvento);
		return new ResponseEntity<Evento>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Evento>  insertar(@RequestBody Evento obj){
		eventoService.insertar(obj);
		return new ResponseEntity<Evento>(obj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> modificar(@RequestBody Evento obj){
		eventoService.modificar(obj);
		ResponseEntity<Object> response = new ResponseEntity<Object>(eventoService, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value="{idEvento}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> borrar(@PathVariable("idEvento") Long idEvento){
		eventoService.eliminar(idEvento);
        	ResponseEntity<Object> response = new ResponseEntity<Object>(idEvento, HttpStatus.OK);
        	return response;
    }
	
}
