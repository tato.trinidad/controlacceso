package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.TipoEvento;
import py.edu.ucsa.controlacceso.core.services.TipoEventoService;

@RestController
@RequestMapping("evtipo")
public class TipoEventoController {

	@Autowired
	private TipoEventoService eventoTipoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<TipoEvento>> listar() {
		List<TipoEvento> respuesta = eventoTipoService.listar();
		return new ResponseEntity<List<TipoEvento>>(respuesta, HttpStatus.OK);
	}

	
	//Metodo GET que recibe como parametro el id del tipo evento para recuperar completamente el mismo
	@RequestMapping(value = "{idtipoEvento}", method = RequestMethod.GET)
	public ResponseEntity<TipoEvento> listarById(@PathVariable("idTipoEvento") Long idTipoEvento) {
		TipoEvento respuesta = eventoTipoService.listarById(idTipoEvento);
		return new ResponseEntity<TipoEvento>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<TipoEvento>  insertar(@RequestBody TipoEvento obj){
		eventoTipoService.insertar(obj);
		return new ResponseEntity<TipoEvento>(obj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> modificar(@RequestBody TipoEvento obj){
		eventoTipoService.modificar(obj);
		ResponseEntity<Object> response = new ResponseEntity<Object>(eventoTipoService, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value="{idTipoEvento}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> borrar(@PathVariable("idTipoEvento") Long idTipoEvento){
		eventoTipoService.eliminar(idTipoEvento);
        	ResponseEntity<Object> response = new ResponseEntity<Object>(idTipoEvento, HttpStatus.OK);
        	return response;
    }
	
	

}
