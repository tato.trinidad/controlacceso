package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.EstadoCivil;
import py.edu.ucsa.controlacceso.core.services.EstadoCivilService;

@RestController
@RequestMapping("ecivil")
public class EstadoCivilController {

	@Autowired
	private EstadoCivilService estadoCivilService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<EstadoCivil>> listar() {
		List<EstadoCivil> respuesta = estadoCivilService.listar();
		return new ResponseEntity<List<EstadoCivil>>(respuesta, HttpStatus.OK);
	}

	
	//Metodo GET que recibe como parametro el id del sexo para recuperar completamente el mismo
	@RequestMapping(value = "{idEstadoCivil}", method = RequestMethod.GET)
	public ResponseEntity<EstadoCivil> listarById(@PathVariable("idEstadoCivil") Long idEstadoCivil) {
		EstadoCivil respuesta = estadoCivilService.listarById(idEstadoCivil);
		return new ResponseEntity<EstadoCivil>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<EstadoCivil>  insertar(@RequestBody EstadoCivil obj){
		estadoCivilService.insertar(obj);
		return new ResponseEntity<EstadoCivil>(obj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> modificar(@RequestBody EstadoCivil obj){
		estadoCivilService.modificar(obj);
		ResponseEntity<Object> response = new ResponseEntity<Object>(estadoCivilService, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value="{idEstadoCivil}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> borrar(@PathVariable("idEstadoCivil") Long idEstadoCivil){
		estadoCivilService.eliminar(idEstadoCivil);
        	ResponseEntity<Object> response = new ResponseEntity<Object>(idEstadoCivil, HttpStatus.OK);
        	return response;
    }
	
	

}
