package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.Cliente;
import py.edu.ucsa.controlacceso.core.services.ClienteService;

@RestController
@RequestMapping("cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Cliente>> listar() {
		List<Cliente> respuesta = clienteService.listar();
		return new ResponseEntity<List<Cliente>>(respuesta, HttpStatus.OK);
	}

	
	//Metodo GET que recibe como parametro el id del sexo para recuperar completamente el mismo
	@RequestMapping(value = "{idCliente}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> listarById(@PathVariable("idCliente") Long idCliente) {
		Cliente respuesta = clienteService.listarById(idCliente);
		return new ResponseEntity<Cliente>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Cliente>  insertar(@RequestBody Cliente obj){
		clienteService.insertar(obj);
		return new ResponseEntity<Cliente>(obj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> modificar(@RequestBody Cliente obj){
		clienteService.modificar(obj);
		ResponseEntity<Object> response = new ResponseEntity<Object>(clienteService, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value="{idCliente}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> borrar(@PathVariable("idCliente") Long idCliente){
		clienteService.eliminar(idCliente);
        	ResponseEntity<Object> response = new ResponseEntity<Object>(idCliente, HttpStatus.OK);
        	return response;
    }
	
	

}
