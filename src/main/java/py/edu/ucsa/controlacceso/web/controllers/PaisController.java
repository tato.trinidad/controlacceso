package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.Pais;
import py.edu.ucsa.controlacceso.core.services.PaisService;

@RestController
@RequestMapping("pais")
public class PaisController {

	@Autowired
	private PaisService paisService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Pais>> listar() {
		List<Pais> respuesta = paisService.listar();
		return new ResponseEntity<List<Pais>>(respuesta, HttpStatus.OK);
	}

	
	//Metodo GET que recibe como parametro el id del sexo para recuperar completamente el mismo
	@RequestMapping(value = "{idPais}", method = RequestMethod.GET)
	public ResponseEntity<Pais> listarById(@PathVariable("idPais") Long idPais) {
		Pais respuesta = paisService.listarById(idPais);
		return new ResponseEntity<Pais>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Pais>  insertar(@RequestBody Pais obj){
		paisService.insertar(obj);
		return new ResponseEntity<Pais>(obj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> modificar(@RequestBody Pais obj){
		paisService.modificar(obj);
		ResponseEntity<Object> response = new ResponseEntity<Object>(paisService, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value="{idPais}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> borrar(@PathVariable("idPais") Long idPais){
		paisService.eliminar(idPais);
        	ResponseEntity<Object> response = new ResponseEntity<Object>(idPais, HttpStatus.OK);
        	return response;
    }
	
	

}
