/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;
import py.edu.ucsa.controlacceso.core.domain.SubSector;

/**
 * @author halfonso
 *
 */
public interface SubSectorMapper {
	public List<SubSector> listar();
	public SubSector listarById(Long idSubSector);
    public void insertar(SubSector obj);
	public void modificar(SubSector obj);
	public void eliminar(long id);
}
