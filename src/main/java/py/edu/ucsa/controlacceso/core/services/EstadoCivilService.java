/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.EstadoCivil;

/**
 * @author halfonso
 *
 */
@Service
public interface EstadoCivilService {
	public List<EstadoCivil> listar();
	public EstadoCivil listarById(Long idEstadoCivil);
    public void insertar(EstadoCivil obj);
	public void modificar(EstadoCivil obj);
	public void eliminar(long id);
	
	
}
