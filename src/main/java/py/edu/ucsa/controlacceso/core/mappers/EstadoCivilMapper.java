/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.EstadoCivil;

/**
 * @author halfonso
 *
 */
public interface EstadoCivilMapper {
	public List<EstadoCivil> listar();
	public EstadoCivil listarById(Long idEstadoCivil);
    public void insertar(EstadoCivil obj);
	public void modificar(EstadoCivil obj);
	public void eliminar(long id);
	
	
}
