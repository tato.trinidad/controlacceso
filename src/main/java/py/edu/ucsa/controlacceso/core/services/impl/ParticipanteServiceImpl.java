/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Participante;
import py.edu.ucsa.controlacceso.core.mappers.ParticipanteMapper;
import py.edu.ucsa.controlacceso.core.services.ParticipanteService;

/**
 * @author halfonso
 *
 */
@Service
public class ParticipanteServiceImpl implements ParticipanteService {

	@Autowired
	private ParticipanteMapper participanteMapper;
	
	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ParticipanteService#listar()
	 */
	@Override
	public List<Participante> listar() {
		// TODO Auto-generated method stub
		return participanteMapper.listar();
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ParticipanteService#listarById(java.lang.Long)
	 */
	@Override
	public Participante listarById(Long idParticipante) {
		// TODO Auto-generated method stub
		return participanteMapper.listarById(idParticipante);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ParticipanteService#insertar(py.edu.ucsa.controlacceso.core.domain.Participante)
	 */
	@Override
	public void insertar(Participante obj) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ParticipanteService#modificar(py.edu.ucsa.controlacceso.core.domain.Participante)
	 */
	@Override
	public void modificar(Participante obj) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ParticipanteService#eliminar(long)
	 */
	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

}
