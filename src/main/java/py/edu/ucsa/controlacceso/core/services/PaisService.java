/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Pais;

/**
 * @author halfonso
 *
 */
@Service
public interface PaisService {
	public List<Pais> listar();
	public Pais listarById(Long idPais);
    public void insertar(Pais obj);
	public void modificar(Pais obj);
	public void eliminar(long id);
}
