/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.EstadoCivil;
import py.edu.ucsa.controlacceso.core.mappers.EstadoCivilMapper;
import py.edu.ucsa.controlacceso.core.services.EstadoCivilService;

/**
 * @author halfonso
 *
 */
@Service
public class EstadoCivilServiceImpl implements EstadoCivilService {

	@Autowired
	private EstadoCivilMapper estadoCivilMapper;
	
	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EstadoCivilService#listar()
	 */
	@Override
	public List<EstadoCivil> listar() {
		// TODO Auto-generated method stub
		return estadoCivilMapper.listar();
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EstadoCivilService#listarById(java.lang.Long)
	 */
	@Override
	public EstadoCivil listarById(Long idEstadoCivil) {
		// TODO Auto-generated method stub
		return estadoCivilMapper.listarById(idEstadoCivil);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EstadoCivilService#insertar(py.edu.ucsa.controlacceso.core.domain.EstadoCivil)
	 */
	@Override
	public void insertar(EstadoCivil obj) {
		// TODO Auto-generated method stub
		estadoCivilMapper.insertar(obj);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EstadoCivilService#modificar(py.edu.ucsa.controlacceso.core.domain.EstadoCivil)
	 */
	@Override
	public void modificar(EstadoCivil obj) {
		// TODO Auto-generated method stub
		estadoCivilMapper.modificar(obj);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EstadoCivilService#eliminar(long)
	 */
	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
		estadoCivilMapper.eliminar(id);
	}

}
