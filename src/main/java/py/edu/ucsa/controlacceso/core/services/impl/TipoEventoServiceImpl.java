/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.TipoEvento;
import py.edu.ucsa.controlacceso.core.mappers.TipoEventoMapper;
import py.edu.ucsa.controlacceso.core.services.TipoEventoService;

/**
 * @author halfonso
 *
 */
@Service
public class TipoEventoServiceImpl implements TipoEventoService {
	
	@Autowired
	private TipoEventoMapper tipoEventoMapper;
	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.TipoEventoService#listar()
	 */
	@Override
	public List<TipoEvento> listar() {
		// TODO Auto-generated method stub
		return tipoEventoMapper.listar();
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.TipoEventoService#listarById(java.lang.Long)
	 */
	@Override
	public TipoEvento listarById(Long idTipoEvento) {
		// TODO Auto-generated method stub
		return tipoEventoMapper.listarById(idTipoEvento);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.TipoEventoService#insertar(py.edu.ucsa.controlacceso.core.domain.TipoEvento)
	 */
	@Override
	public void insertar(TipoEvento obj) {
		// TODO Auto-generated method stub
		tipoEventoMapper.insertar(obj);

	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.TipoEventoService#modificar(py.edu.ucsa.controlacceso.core.domain.TipoEvento)
	 */
	@Override
	public void modificar(TipoEvento obj) {
		// TODO Auto-generated method stub
		tipoEventoMapper.modificar(obj);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.TipoEventoService#eliminar(long)
	 */
	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
		tipoEventoMapper.eliminar(id);

	}

}
