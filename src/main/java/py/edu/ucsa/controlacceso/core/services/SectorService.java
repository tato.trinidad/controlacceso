/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Sector;

/**
 * @author halfonso
 *
 */
@Service
public interface SectorService {
	public List<Sector> listar();
	public Sector listarById(Long idSector);
    public void insertar(Sector obj);
	public void modificar(Sector obj);
	public void eliminar(long id);
}
