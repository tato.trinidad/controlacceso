package py.edu.ucsa.controlacceso.core.domain;

import java.sql.Date;

public class EventoSector {
	private Integer idEventoSector;
	private Evento evento;
	private Sector sector;
	private String capacidad;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private Double precio;
	public Integer getIdEventoSector() {
		return idEventoSector;
	}
	public void setIdEventoSector(Integer idEventoSector) {
		this.idEventoSector = idEventoSector;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	public Sector getSector() {
		return sector;
	}
	public void setSector(Sector sector) {
		this.sector = sector;
	}
	public String getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	
}
