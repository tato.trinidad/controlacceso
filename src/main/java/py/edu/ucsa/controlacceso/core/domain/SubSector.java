package py.edu.ucsa.controlacceso.core.domain;

import java.sql.Date;

public class SubSector {
	private Integer idSubsector;
	private EventoSector eventoSector;
	private String nombre;
	private Date fechaCreacion;
	
	public Integer getIdSubsector() {
		return idSubsector;
	}
	public void setIdSubsector(Integer idSubsector) {
		this.idSubsector = idSubsector;
	}
	public EventoSector getEventoSector() {
		return eventoSector;
	}
	public void setEventoSector(EventoSector eventoSector) {
		this.eventoSector = eventoSector;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	
}
