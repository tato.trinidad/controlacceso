/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.EventoParticipante;

/**
 * @author halfonso
 *
 */
@Service
public interface EventoParticipanteService {
	public List<EventoParticipante> listar();
	public EventoParticipante listarById(Long idEventoParticipante);
    public void insertar(EventoParticipante obj);
	public void modificar(EventoParticipante obj);
	public void eliminar(long id);
	
	
}
