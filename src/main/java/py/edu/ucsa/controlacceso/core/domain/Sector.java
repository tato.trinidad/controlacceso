package py.edu.ucsa.controlacceso.core.domain;

import java.sql.Date;

public class Sector {
	
			private Integer idSector;
			private String nombre;
			private String activo;
			private Date fechaCreacion;
			private String usuarioCreacion;

			public Integer getIdSector() {
				return idSector;
			}

			public void setIdSector(Integer idSector) {
				this.idSector = idSector;
			}

			public String getNombre() {
				return nombre;
			}

			public void setNombre(String nombre) {
				this.nombre = nombre;
			}

			public String getActivo() {
				return activo;
			}

			public void setActivo(String activo) {
				this.activo = activo;
			}

			public Date getFechaCreacion() {
				return fechaCreacion;
			}

			public void setFechaCreacion(Date fechaCreacion) {
				this.fechaCreacion = fechaCreacion;
			}

			public String getUsuarioCreacion() {
				return usuarioCreacion;
			}

			public void setUsuarioCreacion(String usuarioCreacion) {
				this.usuarioCreacion = usuarioCreacion;
			}
			
}
